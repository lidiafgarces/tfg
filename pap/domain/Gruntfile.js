module.exports = function(grunt) {  

  grunt.initConfig({

    copy: {
      build: {
        cwd: 'src',
        src: [ '**', '!lib/*'],
        dest: 'build',
        expand: true
      },
    },

    clean: {
      prev: [ 'build/**/*' ],
      post: [ ]
    },

    jshint: {
	    all: ['Gruntfile.js', 'src/app/***/**/*.js', 'src/public/js/*.js', 'src/viewBuilders/*.js']
	  },


    uglify: {
        demo: {
            files: {
                'build/colors.js': ['src/colors.js'],
                'build/msgbus.js': ['src/msgbus.js'],
            },

        },
        my_target: {
            files: [{
                expand: true,
                cwd: 'src/lib/item',
                src: '*.js',
                dest: 'build/lib/item'
            }]
         }
    },


 
  });

  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-clean');

  grunt.loadNpmTasks('grunt-contrib-uglify');

  grunt.loadNpmTasks('grunt-contrib-jshint');


  // define the tasks
   
  grunt.registerTask(
    'build', 
    'Compiles all of the assets, validate js files, annotate angular and minify files to the build directory.', 
    ['clean:prev', 'copy', 'jshint', 'uglify', 'clean:post']
  );


};