module.exports = function(grunt) {  

  grunt.initConfig({

    copy: {
      build: {
        cwd: 'src',
        src: [ '**', '!app/views/*.jade', '!public/css/*.css' ],
        dest: 'build',
        expand: true
      },
    },

    clean: {
      prev: [ 'build/**/*' ],
      post: [ ]
    },

    jshint: {
	    all: ['Gruntfile.js', 'src/app/***/**/*.js', 'src/public/js/*.js', 'src/viewBuilders/*.js']
	  },

    htmlmin: {                                     // Task 
      dist: {                                      // Target 
        options: {                                 // Target options 
          removeComments: true,
          collapseWhitespace: true
        },
                                           // Dictionary of files 
          files: [{
           expand: true,
           cwd: 'build',
           src: ['app/**/*.tpl.html' ],
           dest: 'build/'
        }]
        
      }
    },

    concat: {
      libs: {
        src: ['src/public/js/lib/underscore.js', 'src/public/js/lib/backbone.js', 'src/public/js/lib/backbone-pageable.js', 'src/public/js/lib/backboneCQRS-0.4.js', 'src/public/js/lib/backgrid.js', 'src/public/js/lib/backgrid-paginator.js', 'src/public/js/lib/backbone.localStorage.js', 'src/public/js/lib/bootstrap.min.js', 'src/public/js/lib/recline.js' ],
        dest: 'build/public/js/lib/libs.js',
      },
    },

    uglify: {
        demo: {
            files: {
                'build/colors.min.js': ['build/colors.js'],
                'build/msgbus.min.js': ['build/msgbus.js']
            }
        }
    },


    cssmin: {
      options: {
        shorthandCompacting: false,
        roundingPrecision: -1
      },
      target: {
        files: {
          'build/public/css/style.css': ['src/public/css/*']
        }
      },
      min: {
        files: [{
          expand: true,
          cwd: 'build/public/css',
          src: ['style.css'],
          dest: 'build/public/css',
          ext: '.min.css'
        }]
      }
    },

    jade: {
      html: {
        files: {
          'build/app/views': ['src/app/views/*.jade']
        },
        options: {
          client: false
        }
      }
    }
 
  });

  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-clean');

  grunt.loadNpmTasks('grunt-contrib-uglify');

  grunt.loadNpmTasks('grunt-ng-annotate');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-concat');

  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-cssmin');

  grunt.loadNpmTasks('grunt-angular-templates');

  grunt.loadNpmTasks('grunt-jade');

  // define the tasks
   
  grunt.registerTask(
    'build', 
    'Compiles all of the assets, validate js files, annotate angular and minify files to the build directory.', 
    ['clean:prev', 'copy', 'jshint', 'htmlmin', 'uglify', 'cssmin', 'jade', 'concat', 'clean:post']
  );


};