var myApp = angular.module('myApp', ['ngRoute']);

myApp.config(["$routeProvider", function ($routeProvider) {
  $routeProvider
    .when('/', {
      templateUrl: 'partials/home.html',
      controller: 'homeController',
      access: {restricted: true}
    })
    .when('/login', {
      templateUrl: 'partials/login.html',
      controller: 'loginController',
      access: {restricted: false}
    })
    .when('/logout', {
      controller: 'logoutController',
      access: {restricted: true}
    })
    .when('/register', {
      templateUrl: 'partials/register.html',
      controller: 'registerController',
      access: {restricted: true}
    })
    .when('/edit/:username', {
      templateUrl: 'partials/edit.html',
      controller: 'editController',
      access: {restricted: true}
    })
    .when('/reset/:username', {
      templateUrl: 'partials/reset.html',
      controller: 'resetController',
      access: {restricted: true}
    })
    .when('/one', {
      template: '<h1>This is page one!</h1>',
      access: {restricted: true}
    })
    .when('/two', {
      template: '<h1>This is page two!</h1>',
      access: {restricted: false}
    })
    .otherwise({
      redirectTo: '/'
    });
}]);

myApp.run(["$rootScope", "$location", "$route", "AuthService", function ($rootScope, $location, $route, AuthService) {
  $rootScope.$on('$routeChangeStart',
    function (event, next, current) {
      //AuthService.getUserStatus();
      if (next.access.restricted &&
          !AuthService.isLoggedIn()) {
        $location.path('/login');
        $route.reload();
      }
  });
}]);
angular.module('myApp').factory('AuthService', ['$q', '$timeout', '$http',
    function($q, $timeout, $http) {
        console.log('User: ' + localStorage.getItem('user'));
        // create user variable
        var user = localStorage.getItem('user');
        //localStorage.setItem('user', null);

        // return available functions for use in the controllers
        return ({
            isLoggedIn: isLoggedIn,
            getUserStatus: getUserStatus,
            getUsers: getUsers,
            getUser: getUser,
            login: login,
            logout: logout,
            register: register,
            edit: edit,
            reset: reset,
            deleteUser: deleteUser
        });

        function isLoggedIn() {
            console.log(localStorage.getItem('user'));
            var test = localStorage.getItem('user');
            if (test) {
                return true;
            } else {
                console.log('pues no entra');
                return false;
            }
        }

        function getUserStatus() {
            return $http.get('/api/status')
                // handle success
                .success(function(data) {
                    //console.log(data);
                    if (data.status) {
                        user = true;
                    } else {
                        user = false;
                    }
                    localStorage.setItem('user', user);
                })
                // handle error
                .error(function(data) {
                    user = false;
                    localStorage.setItem('user', user);
                });
        }

        function getUsers() {
            return $http.get('/api/users')
                // handle success
                .success(function(data) {
                    //console.log(data);
                    if (data.status) {
                        user = true;
                    } else {
                        user = false;
                    }
                    localStorage.setItem('user', user);
                })
                // handle error
                .error(function(data) {
                    user = false;
                    localStorage.setItem('user', user);
                });
        }

        function login(username, password) {

            // create a new instance of deferred
            var deferred = $q.defer();

            // send a post request to the server
            $http.post('/api/login', { username: username, password: password })
                // handle success
                .success(function(data, status) {
                    if (status === 200 && data.status) {
                        user = true;
                        localStorage.setItem('user', user);
                        deferred.resolve();
                    } else {
                        user = false;
                        localStorage.setItem('user', user);
                        deferred.reject();
                    }
                })
                // handle error
                .error(function(data) {
                    user = false;
                    localStorage.setItem('user', user);
                    deferred.reject();
                });

            // return promise object
            return deferred.promise;

        }

        function logout() {

            // create a new instance of deferred
            var deferred = $q.defer();

            // send a get request to the server
            $http.get('/api/logout')
                // handle success
                .success(function(data) {
                    user = false;
                    localStorage.setItem('user', user);
                    deferred.resolve();
                })
                // handle error
                .error(function(data) {
                    user = false;
                    localStorage.setItem('user', user);
                    deferred.reject();
                });

            // return promise object
            return deferred.promise;

        }

        function deleteUser(username) {

            // create a new instance of deferred
            var deferred = $q.defer();
            //console.log(username);

            // send a post request to the server
            $http.delete('/api/delete/' + username)
                // handle success
                .success(function(data, status) {
                    if (status === 200 && data.status) {
                        user = true;
                        localStorage.setItem('user', user);
                        deferred.resolve();
                    } else {
                        user = false;
                        localStorage.setItem('user', user);
                        deferred.reject();
                    }
                })
                // handle error
                .error(function(data) {
                    user = false;
                    localStorage.setItem('user', user);
                    deferred.reject();
                });

            // return promise object
            return deferred.promise;

        }


        function register(form) {

            // create a new instance of deferred
            var deferred = $q.defer();
            console.log(form);

            // send a post request to the server
            $http.post('/api/register',
                    form)
                // handle success
                .success(function(data, status) {
                    if (status === 200 && data.status) {
                        deferred.resolve();
                    } else {
                        deferred.reject();
                    }
                })
                // handle error
                .error(function(data) {
                    deferred.reject();
                });

            // return promise object
            return deferred.promise;

        }

        function edit(form) {

            // create a new instance of deferred
            var deferred = $q.defer();
            //console.log(form);

            // send a post request to the server
            $http.post('/api/edit',
                    form)
                // handle success
                .success(function(data, status) {
                    if (status === 200 && data.status) {
                        deferred.resolve();
                    } else {
                        deferred.reject();
                    }
                })
                // handle error
                .error(function(data) {
                    deferred.reject();
                });

            // return promise object
            return deferred.promise;

        }

        function reset(form) {

            // create a new instance of deferred
            var deferred = $q.defer();
            //console.log(form);

            // send a post request to the server
            $http.post('/api/reset',
                    form)
                // handle success
                .success(function(data, status) {
                    if (status === 200 && data.status) {
                        deferred.resolve();
                    } else {
                        deferred.reject();
                    }
                })
                // handle error
                .error(function(data) {
                    deferred.reject();
                });

            // return promise object
            return deferred.promise;

        }

        function getUser(username) {

            // create a new instance of deferred
            var deferred = $q.defer();
            console.log(username);

            // send a post request to the server
            return $http.get('/api/users/' + username)
                // handle success
                .success(function(data, status) {
                    if (status === 200 && data.status) {
                        deferred.resolve();
                    } else {
                        deferred.reject();
                    }
                })
                // handle error
                .error(function(data) {
                    deferred.reject();
                });

        }


    }
]);

angular.module('myApp').controller('loginController', ['$scope', '$location', 'AuthService',
    function($scope, $location, AuthService) {

        $scope.login = function() {

            // initial values
            $scope.error = false;
            $scope.disabled = true;

            // call login from service
            AuthService.login($scope.loginForm.username, $scope.loginForm.password)
                // handle success
                .then(function() {
                    $location.path('/');
                    $scope.disabled = false;
                    $scope.loginForm = {};
                })
                // handle error
                .catch(function() {
                    $scope.error = true;
                    $scope.errorMessage = "Invalid username and/or password";
                    $scope.disabled = false;
                    $scope.loginForm = {};
                });

        };

    }
]);

/*angular.module('myApp').controller('logoutController', ['$scope', '$location', 'AuthService',
    function($scope, $location, AuthService) {

        $scope.logout = function() {

            // call logout from service
            AuthService.logout()
                .then(function() {
                    $location.path('/login');
                });

        };

    }
]);*/

angular.module('myApp').controller('deleteUserController', ['$scope', '$location', 'AuthService',
    function($scope, $location, AuthService) {

        $scope.deleteUser = function() {

            // call logout from service
            AuthService.deleteUser()
                .then(function() {
                    $location.path('/');
                });

        };

    }
]);

angular.module('myApp').controller('homeController', ['$scope', '$location', 'AuthService',
    function($scope, $location, AuthService) {

        AuthService.getUsers()
            .then(function(response) {
                console.log(response.data.status);
                if (!response.data.status) $location.path('/login');
                console.log(response.data.users);
                $scope.users = response.data.users;
            });

        $scope.newUser = function() {
            $location.path('/register');
        };

        $scope.editUser = function(username) {
            $location.path('/edit/' + username);
        };

        $scope.resetPassword = function(username) {
            $location.path('/reset/' + username);
        };


        $scope.logout = function() {

            // call logout from service
            AuthService.logout()
                .then(function() {
                    $location.path('/login');
                });

        };

        $scope.deleteUser = function(username) {
            console.log(username);

            // call logout from service
            AuthService.deleteUser(username).then(function() {
                AuthService.getUsers().then(function(response) {
                    console.log(response.data.users);
                    $scope.users = response.data.users;
                });
            });

        };

    }
]);

angular.module('myApp').controller('registerController', ['$scope', '$location', 'AuthService',
    function($scope, $location, AuthService) {

        $scope.register = function() {

            // initial values
            $scope.error = false;
            $scope.disabled = true;

            // call register from service
            AuthService.register($scope.registerForm)
                // handle success
                .then(function() {
                    $location.path('/');
                    $scope.disabled = false;
                    $scope.registerForm = {};
                })
                // handle error
                .catch(function() {
                    $scope.error = true;
                    $scope.errorMessage = "Something went wrong!";
                    $scope.disabled = false;
                    $scope.registerForm = {};
                });

        };

    }
]);

angular.module('myApp').controller('editController', ['$scope', '$route', '$routeParams', '$location', 'AuthService',
    function($scope, $route, $routeParams, $location, AuthService) {
        AuthService.getUser($routeParams.username)
            // handle success
            .then(function(response) {
                console.log(response.data);
                $scope.registerForm = response.data.user;
            })
            // handle error
            .catch(function() {
                $scope.error = true;
                $scope.errorMessage = "Something went wrong!";
                $scope.disabled = false;
            });

        $scope.edit = function() {

            // initial values
            $scope.error = false;
            $scope.disabled = true;

            // call register from service

            AuthService.edit($scope.registerForm)
                // handle success
                .then(function() {
                    $location.path('/');
                    $scope.disabled = false;
                    $scope.registerForm = {};
                })
                // handle error
                .catch(function() {
                    $scope.error = true;
                    $scope.errorMessage = "Something went wrong!";
                    $scope.disabled = false;
                    //$scope.registerForm = {};
                });
        };

    }
]);

angular.module('myApp').controller('resetController', ['$scope', '$route', '$routeParams', '$location', 'AuthService',
    function($scope, $route, $routeParams, $location, AuthService) {
        $scope.resetForm = {};
        $scope.resetForm.username = $routeParams.username;
        $scope.reset = function() {

            // initial values
            $scope.error = false;
            $scope.disabled = true;

            // call register from service
            if ($scope.resetForm.password === $scope.resetForm.repeatPassword) {
                delete $scope.resetForm.repeatPassword;
                AuthService.reset($scope.resetForm)
                    // handle success
                    .then(function() {
                        $location.path('/');
                        $scope.disabled = false;
                        $scope.resetForm = {};
                    })
                    // handle error
                    .catch(function() {
                        console.log();
                        $scope.error = true;
                        $scope.errorMessage = "Something went wrong!";
                        $scope.disabled = false;
                        //$scope.registerForm = {};
                    });
            }else{
              console.log('No coinciden');
            }

        };

    }
]);