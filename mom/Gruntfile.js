module.exports = function(grunt) {  

  grunt.initConfig({

    copy: {
      build: {
        cwd: 'src',
        src: [ '**', '!client/*.js', '!partials/libs', ],
        dest: 'build',
        expand: true
      },
    },

    clean: {
      prev: [ 'build/**/*' ],
      post: [ ]
    },

    jshint: {
	    all: ['Gruntfile.js', 'src/client/*.js']
	  },

    htmlmin: {                                     // Task 
      dist: {                                      // Target 
        options: {                                 // Target options 
          removeComments: true,
          collapseWhitespace: true
        },
                                           // Dictionary of files 
          files: [{
           expand: true,
           cwd: 'build',
           src: ['src/client/**/*.tpl.html' ],
           dest: 'build/'
        }]
        
      }
    },

    concat: {
      libs: {
        src: ['src/client/partials/libs/jquery-2.2.1.js', 'src/client/partials/libs/bootstrap.js', 'src/client/partials/libs/angular.js', 'src/client/partials/libs/angular-route.js' ],
        dest: 'build/client/libs.js',
      },
    },

    uglify: {
        my_target: {
            files: [{
                expand: true,
                cwd: 'src/client',
                src: '*.js',
                dest: 'build/client'
            }]
        }
    },

    ngAnnotate: {
        app: {
            files: {
                'build/client/app.js': ['src/client/main.js', 'src/client/services.js', 'src/client/controllers.js' ]
            },
        },
        libs: {
            files: {
                'build/client/libs.js': ['build/client/libs.js']
            },
        }
    },
 
  });

  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-clean');

  grunt.loadNpmTasks('grunt-contrib-uglify');

  grunt.loadNpmTasks('grunt-contrib-htmlmin');

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-concat');

  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-ng-annotate');

  // define the tasks
   
  grunt.registerTask(
    'build', 
    'Compiles all of the assets, validate js files, annotate angular and minify files to the build directory.', 
    ['clean:prev', 'copy', 'jshint', 'htmlmin', 'concat', 'ngAnnotate', 'clean:post']
  );


};