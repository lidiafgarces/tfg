(function(){

	angular.module('starter')
	.service('SocketService', ['socketFactory', SocketService]);

	function SocketService(socketFactory){
		return socketFactory({
			
			ioSocket: io.connect('http://localhost:3000')
			//ioSocket: io.connect('http://ie-server-tfg.cfapps.io')

		});
	}
})();