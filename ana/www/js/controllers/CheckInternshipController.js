(function() {
    angular.module('starter')
        .controller('CheckInternshipController', ['$scope', '$state', '$stateParams', '$http', '$ionicPopup', '$timeout', 'localStorageService', 'SocketService', CheckInternshipController]);

    function CheckInternshipController($scope, $state, $stateParams, $http, $ionicPopup, $timeout, localStorageService, SocketService) {

        var me = this;
        console.log('CheckInternshipController');

        me.current_room = localStorageService.get('room');
        $scope.internship = {};
        $scope.internship.exchanged = false;
        $scope.internship.member = '';

        $scope.getInternship = function(ref) {
            $http({
                method: 'GET',
                url: 'http://pap-host-tfg.cfapps.io/api/internship/' + ref,
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                //console.log(response.data)
                //$scope.internship = response.data;

                $scope.internship.ref = ((response.data && response.data.ref) ? response.data.ref : "");
                $scope.internship.text = ((response.data && response.data.text) ? response.data.text : "");
                //$scope.internship.text = response.data.text ? '';

                SocketService.emit('get:internship', $scope.internship.ref);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log('Error:');
                console.log(response);
            });
        };

        SocketService.on('intItem', function(exist, intItem) {
            console.log('El socket nos dice que:')
            console.log(exist);
            var notFound = true;
            if (!exist) return $scope.showPopup(true);
            $scope.internship.exchanged = ((intItem && intItem.exchanged) ? intItem.exchanged : false);
            //$scope.internship.exchanged = intItem.exchanged;
            $scope.internship.member = ((intItem && intItem.member) ? intItem.member : '');
            $scope.$apply();
        });


        $scope.showPopup = function(notFound) {
            $scope.data = {};
            $scope.data.ref = $stateParams.ref;
            console.log('Encontrado:');
            console.log(notFound);
            $scope.data.test = notFound;
            console.log($scope.data.test);

            // An elaborate, custom popup
            var myPopup = $ionicPopup.show({
                template: '<div ng-show="data.test" style="color:red">Reference not found!</div><input type="text" ng-model="data.ref">',
                title: 'Enter Internship Reference',
                subTitle: 'Please use normal things',
                scope: $scope,
                buttons: [
                    { text: 'Cancel' }, {
                        text: '<b>Search</b>',
                        type: 'button-positive',
                        onTap: function(e) {
                            console.log('Datos:');
                            console.log($scope.data.ref);
                            if (!$scope.data.ref) {
                                //don't allow the user to close unless he enters wifi password
                                e.preventDefault();
                            } else {
                                $scope.getInternship($scope.data.ref);
                                return $scope.data.ref;
                            }
                        }
                    }
                ]
            });

            myPopup.then(function(res) {
                console.log('Tapped!', res);
                if (res) {
                    myPopup.close();
                } else {
                    $scope.leaveRoom();
                }
            });

            $timeout(function() {
                if (!$scope.data.ref) {
                    myPopup.close();
                    $scope.leaveRoom();
                }
            }, 60000);
        };
        $scope.showPopup();

        $scope.showEditPopup = function() {

            // An elaborate, custom popup
            var myPopup = $ionicPopup.show({
                //template: '<div ng-show="internship.exchanged">This internship is exchanged.</div><div ng-hide="internship.exchanged">This internship is not exchanged</div>',
                title: 'Is the internship exchanged?',
                subTitle: 'This will not update the specialization list',
                scope: $scope,
                buttons: [
                    { text: 'Yes' }, {
                        text: '<b>No</b>',
                        type: 'button-positive',
                        onTap: function(e) {
                            return true;
                        }
                    }
                ]
            });

            myPopup.then(function(res) {
                console.log('Tapped!', res);
                if (res) {
                    $scope.internship.exchanged = false;
                    SocketService.emit('update:internship', $scope.internship);
                    myPopup.close();
                } else {
                    $scope.internship.exchanged = true;
                    $scope.internship.member = localStorageService.get('username');
                    myPopup.close();
                    SocketService.emit('update:internship', $scope.internship);
                }
            });

            $timeout(function() {
                if (!$scope.data.ref) {
                    myPopup.close();
                    $scope.leaveRoom();
                }
            }, 60000);
        };


        $scope.enterRoom = function(room_name) {

            me.current_room = room_name;
            localStorageService.set('room', room_name);

            if (room_name === 'State Specializations') {
                $state.go('spcList');

            } else if (room_name === 'Edit Specializations') {
                $state.go('spcEdit');
            } else {
                var room = {
                    'room_name': room_name
                };

                SocketService.emit('join:room', room);

                $state.go('room');

            }


        };

        $scope.leaveRoom = function() {

            $state.go('rooms');

        };

    }

})();
