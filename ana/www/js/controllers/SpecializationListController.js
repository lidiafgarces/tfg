(function() {
    angular.module('starter')
        .controller('SpecializationListController', ['$scope', '$state', 'localStorageService', 'SocketService', 'moment', '$ionicScrollDelegate', '$ionicPopup', '$http', SpecializationListController]);

    function SpecializationListController($scope, $state, localStorageService, SocketService, moment, $ionicScrollDelegate, $ionicPopup, $http) {

        var me = this;
        console.log('dentro jeje');
        SocketService.emit('get:specializations');
        SocketService.emit('get:internships');

        me.specializationItems = [];
        $scope.items = [{ name: 'lala' }];

        $scope.data = {
            listCanSwipe: true
        };

        var current_user = localStorageService.get('username');

        $http({
            method: 'GET',
            url: 'http://pap-host-tfg.cfapps.io/api/allItems.json',
        }).then(function successCallback(response) {
            // this callback will be called asynchronously
            // when the response is available
            console.log('La respuesta es:');
            console.log(response.data);
            var references = [];
            angular.forEach(response.data, function(value, key) {
                console.log(value.ref);
                references.push(value.ref);
                SocketService.emit('get:internship', value.ref);
            });
            //var references = response.data;
            console.log(references);
            console.log('Emitimos al socket...');
            SocketService.emit('get:internships', references);
            me.references = references;
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            console.log('Error:');
            console.log(response);
        });

        $scope.sendTextMessage = function(){

            var msg = {
                'user': current_user,
                'text': me.message,
                'time': moment()
            };

            me.message = '';
            
            SocketService.emit('send:message', msg);
        };

        $scope.showPopup = function(warnings) {
            $scope.data = {};
            $scope.data.member = localStorageService.get('username');
            console.log(warnings);
            if (warnings) {
                $scope.data.spcWarning = warnings.spcWarning;
                $scope.data.intWarning = warnings.intWarning;
            };


            // An elaborate, custom popup
            var myPopup = $ionicPopup.show({
                template: '<div ng-show="data.spcWarning" style="color:red">Specialization not found!</div><div ng-show="data.intWarning" style="color:red">Reference not found!</div>Exchanged reference:<input type="text" ng-model="data.ref"> Gotten specialization:<input type="text" ng-model="data.newSpc">',
                title: 'Enter Specialization',
                subTitle: 'Choose the specialization for the internship gotten',
                scope: $scope,
                buttons: [
                    { text: 'Cancel' }, {
                        text: '<b>Save</b>',
                        type: 'button-positive',
                        onTap: function(e) {
                            if (!$scope.data.newSpc || !$scope.data.ref) {
                                //don't allow the user to close unless he enters wifi password
                                e.preventDefault();
                            } else {
                                return $scope.data;
                            }
                        }
                    }
                ]
            });
            myPopup.then(function(res) {
                if (!res) return myPopup.close();
                console.log(res);
                SocketService.emit('update:specializations', res);
                console.log('Hemos introducido la especialidad ', res);
            });
        };

        $scope.leaveRoom = function() {

            $state.go('rooms');

        };


        SocketService.on('list', function(spcItems) {
            console.log('Nuestros items guardados son:');
            angular.forEach(spcItems, function(value, key) {
                console.log(value);
            });
            me.specializationItems = spcItems;
            console.log('Nuestros items locales son:');
            angular.forEach($scope.items, function(value, key) {
                console.log(value);
            });
            $scope.$apply();
        });

        SocketService.on('update', function(warnings) {
            if (warnings) return $scope.showPopup(warnings);
            SocketService.emit('get:specializations');
        });


    }

})();
