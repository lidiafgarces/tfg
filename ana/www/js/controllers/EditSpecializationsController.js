(function() {
    angular.module('starter')
        .controller('EditSpecializationsController', ['$scope', '$state', 'localStorageService', 'SocketService', 'moment', '$ionicScrollDelegate', '$ionicPopup', EditSpecializationController]);

    function EditSpecializationController($scope, $state, localStorageService, SocketService, moment, $ionicScrollDelegate, $ionicPopup) {

        var me = this;

        SocketService.emit('get:specializations');
        $scope.items = [{ name: 'lala' }];
        me.specializationItems = [{ name: 'lala' }];

        $scope.data = {
            showDelete: false,
            listCanSwipe: true
        };

        $scope.shouldShowDelete = false;
        $scope.listCanSwipe = true;


        var current_user = localStorageService.get('username');

        $scope.new = function() {
            $scope.data = {};

            // An elaborate, custom popup
            var myPopup = $ionicPopup.show({
                template: 'Specialization<input type="text" ng-model="data.newSpc.name">Target <input type="text" ng-model="data.newSpc.target"> Current<input type="text" ng-model="data.newSpc.current">',
                title: 'Enter Details',
                subTitle: 'Enter the details about the specialization',
                scope: $scope,
                buttons: [
                    { text: 'Cancel' }, {
                        text: '<b>Save</b>',
                        type: 'button-positive',
                        onTap: function(e) {
                            if (!$scope.data.newSpc) {
                                //don't allow the user to close unless he enters wifi password
                                e.preventDefault();
                            } else {
                                return $scope.data;
                            }
                        }
                    }
                ]
            });
            myPopup.then(function(res) {
                console.log('Hemos introducido la especialidad ', res);
                SocketService.emit('add:specialization', res.newSpc);
            });
        };

        $scope.edit = function(item) {
            $scope.data = {};
            $scope.data.editSpc = item

            // An elaborate, custom popup
            var myPopup = $ionicPopup.show({
                template: 'Specialization<input type="text" ng-model="data.editSpc.name">Target <input type="text" ng-model="data.editSpc.target"> Current<input type="text" ng-model="data.editSpc.current">',
                title: 'Enter Details',
                subTitle: 'Edit the details about the specialization',
                scope: $scope,
                buttons: [
                    { text: 'Cancel' }, {
                        text: '<b>Save</b>',
                        type: 'button-positive',
                        onTap: function(e) {
                            if (!$scope.data.editSpc) {
                                //don't allow the user to close unless he enters wifi password
                                e.preventDefault();
                            } else {
                                return $scope.data.editSpc;
                            }
                        }
                    }
                ]
            });
            myPopup.then(function(item) {
                console.log('Item editado: ', item);
                SocketService.emit('edit:specialization', item);
                $state.go($state.current, {}, {reload: true});
            });
        };


        $scope.delete = function(item) {
            console.log('borrar');
            SocketService.emit('edit:specialization', item);
        };

        $scope.leaveRoom = function() {

            $state.go('rooms');

        };


        SocketService.on('list', function(spcItems) {
            console.log('Nuestros items guardados son:');
            angular.forEach(spcItems, function(value, key) {
                console.log(value);
            });
            me.specializationItems = spcItems;
            console.log('Nuestros items locales son:');
            angular.forEach($scope.items, function(value, key) {
                console.log(value);
            });
            $scope.$apply();
        });

        SocketService.on('update', function(spcItems) {
            SocketService.emit('get:specializations');
        });


    }

})();
