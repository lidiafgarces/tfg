(function() {
    angular.module('starter')
        .controller('ListInternshipsController', ['$scope', '$state', '$http', 'localStorageService', 'SocketService', ListInternshipsController]);

    function ListInternshipsController($scope, $state, $http, localStorageService, SocketService) {

        var me = this;
        console.log('ListInternshipsController');

        me.current_room = localStorageService.get('room');
        me.rooms = ['Chat', 'State Specializations', 'Edit Specializations', 'Check Internship', 'State Insthernships', 'Photography'];
        me.references = [];
        me.internships = [];
        $http({
            method: 'GET',
            url: 'http://pap-host-tfg.cfapps.io/api/allReferences.json',
        }).then(function successCallback(response) {
            // this callback will be called asynchronously
            // when the response is available
            console.log(response.data);
            var references = response.data;
            console.log('Emitimos al socket...');
            SocketService.emit('get:internships', references);
            me.references = references;
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            console.log('Error:');
            console.log(response);
        });

        SocketService.on('intList', function(intItems) {
            me.internships = [];
            console.log('');
            console.log('Nuestros items procesados son:');
            console.log(intItems);
            angular.forEach(me.references, function(refValue, refKey) {
                console.log('Para la referencia ' + refValue + ':');
                var intItem = {
                    ref: refValue,
                    exchanged: false
                }
                console.log('Inserto ' + refValue);
                me.internships.push(intItem);
                for (var i = 0; i < intItems.length; i++) {
                    //angular.forEach(intItems, function(intValue, intKey) {
                    /*console.log(intItems[i].ref);
                    console.log(refValue);
                    console.log('y ');
                    console.log(me.internships[refKey]);*/
                    
                    if (intItems[i].ref === refValue) {
                        me.internships[refKey].exchanged = true;
                        console.log('La práctica '+ me.internships[refKey].ref + 'está ' + me.internships[refKey].exchanged);
                        break;
                    }
                    
                    //});
                };
            });
            console.log('Nuestros refs totales son:');
            angular.forEach(me.internships, function(value, key) {
                //console.log(value);
            });
            $scope.$apply();
        });

        /*SocketService.on('intList', function(intItems) {
            SocketService.emit('get:internships', me.references);
        });*/

        $scope.login = function(username, password) {
            localStorageService.set('username', username);
            $http({
                method: 'POST',
                url: 'http://mom-tfg.cfapps.io/api/login',
                data: {
                    username: username,
                    password: password
                }
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                console.log(response)
                $state.go('rooms');
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log('Error:');
                console.log(response);
            });
            //$state.go('rooms');
        };


        $scope.enterRoom = function(room_name) {

            me.current_room = room_name;
            localStorageService.set('room', room_name);

            if (room_name === 'State Specializations') {
                $state.go('spcList');

            } else if (room_name === 'Edit Specializations') {
                $state.go('spcEdit');
            } else {
                var room = {
                    'room_name': room_name
                };

                SocketService.emit('join:room', room);

                $state.go('room');

            }


        };

        $scope.showInternship = function(ref) {

            $state.go('intCheck',{ref:ref});

        };

        $scope.leaveRoom = function() {

            $state.go('rooms');

        };

    }

})();
