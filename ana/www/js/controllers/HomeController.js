(function() {
    angular.module('starter')
        .controller('HomeController', ['$scope', '$state', '$http', 'localStorageService', 'SocketService', HomeController]);

    function HomeController($scope, $state, $http, localStorageService, SocketService) {

        var me = this;

        me.current_room = localStorageService.get('room');
        me.rooms = ['Chat', 'State Specializations', 'Edit Specializations', 'Show Internship', 'State Internships'];


        $scope.login = function(username, password) {
            localStorageService.set('username', username);
            $http({
                method: 'POST',
                url: 'http://mom-tfg.cfapps.io/api/login',
                data: {
                    username: username,
                    password: password
                }
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                console.log(response.data.user.role)
                console.log((response.data.user.role === 'secretary'));
                console.log((response.data.user.role === 'representative'));
                if ((response.data.user.role === 'secretary') || (response.data.user.role === 'representative')){
                    $state.go('rooms');
                }else{
                    console.log('error');
                }
                
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log('Error:');
                console.log(response);
            });
            //$state.go('rooms');
        };


        $scope.enterRoom = function(room_name) {

            me.current_room = room_name;
            localStorageService.set('room', room_name);

            if (room_name === 'State Specializations') {
                $state.go('spcList');

            } else if (room_name === 'Edit Specializations') {
                $state.go('spcEdit');
            } else if (room_name === 'Show Internship') {
                $state.go('intCheck');
            } else if (room_name === 'State Internships') {
                $state.go('intList');
            } else {
                var room = {
                    'room_name': room_name
                };

                SocketService.emit('join:room', room);

                $state.go('room');

            }

        };

    }

})();
