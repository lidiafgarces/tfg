// user model
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var InternshipItem = new Schema({
  ref: String,
  exchanged: Boolean,
  member: String
});


module.exports = mongoose.model('internshipItems', InternshipItem);