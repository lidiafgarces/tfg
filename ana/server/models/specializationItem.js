// user model
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var SpecializationItem = new Schema({
  name: String,
  target: String,
  current: String,
});


module.exports = mongoose.model('specializationItems', SpecializationItem);