// user model
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var MessageItem = new Schema({
  user: String,
  text: String,
  time: String,
});

module.exports = mongoose.model('messageItems', MessageItem);