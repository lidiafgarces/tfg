var mongoose = require('mongoose');
mongoose.connect('mongodb://CloudFoundry_5b904c6m_in64f6if_inh09d35:ojeLV7hZuoU_vVIUZJONZxQiwHhWZRT9@ds031608.mlab.com:31608/CloudFoundry_5b904c6m_in64f6if');
//mongoose.connect('mongodb://localhost/chat');
var SpecializationItem = require('./models/specializationItem.js');
var InternshipItem = require('./models/internshipItem.js');
var MessageItem = require('./models/messageItem.js');

//var io = require('socket.io')(4443, {origins:'http://192.168.1.115:*' '*192.168.1.115:*' '192.168.1.115:*'});
var io = require('socket.io')(3000);

//io.set("origins","*:*");

var specializationsList = [];

io.on('connection', function(socket) {

    socket.on('join:room', function() {
        //var room_name = data.room_name;
        //socket.join(room_name);
        console.log('entramos');
        MessageItem.find({}, function(err, messageItems) {
            if (err) return handleError(err);
            console.log('Mis items son: ' + messageItems);
            socket.emit('messages', messageItems);
        })
    });

    socket.on('leave:room', function(msg) {
        msg.text = msg.user + ' has left the room';
        socket.leave(msg.room);
        socket.in(msg.room).emit('message', msg);
    });

    /*socket.on('get:messages', function() {
        console.log('vienen mensajes');
        MessageItem.find({}, function(err, messageItems) {
            if (err) return handleError(err);
            console.log('Mis items son: ' + messageItems);
            socket.emit('list', messageItems);
        })
    });*/

    socket.on('send:message', function(msg) {
        var newMessage = new MessageItem(msg);
        newMessage.save(function (err, newMsg) {
          if (err) return console.error(err);
          console.log(newMsg);
          socket.in(msg.room).emit('message', newMsg);
        });
        /*MessageItem.findOneAndUpdate({
            name: item.name
        }, item, function(err, spcItem) {
            if (err) return handleError(err);
            console.log('Mi nuevo item es: ' + spcItem);
            socket.emit('list:messages', msgItems);
        });*/

    });

    socket.on('get:specializations', function() {
        console.log('estoy in');
        SpecializationItem.find({}, function(err, spcItems) {
            if (err) return handleError(err);
            specializationsList = spcItems;
            console.log('Mis items son: ' + spcItems);
            socket.emit('list', spcItems);
        })
    });

    socket.on('add:specialization', function(item) {
        SpecializationItem.findOneAndUpdate({
            name: item.name
        }, item, {
            upsert: true
        }, function(err, spcItem) {
            if (err) return handleError(err);
            console.log('Mi nuevo item es: ' + spcItem);
            socket.emit('update');
        })
    });

    socket.on('delete:specialization', function(item) {
        SpecializationItem.find({
            name: item.name
        }).remove().exec();
        socket.emit('update');
    });

    socket.on('update:specializations', function(data) {
        //var updatedSpcItem = new SpecializationItem(item);
        var newItem = [];
        console.log('Para actualizar especializacion');
        console.log(data);
        SpecializationItem.findOne({
            name: data.newSpc
        }, function(err, oldItem) {
            if (err) return handleError(err);
            console.log('La referencia ya existe y es: '+oldItem.ref)
            InternshipItem.find({},function(err, todos) {
                console.log(todos);
            });
            InternshipItem.findOne({
                ref: data.ref
            }, function(err, existingItem) {
                console.log('Especializacion encontrada:');
                console.log(oldItem);
                console.log(existingItem);
                if (err) return console.log(err);
                if (!oldItem || existingItem) return socket.emit('update', {
                    spcWarning: oldItem !== null,
                    intWarning: existingItem === null
                });

                console.log('Mi item es: ');
                console.log(oldItem);
                newItem = JSON.parse(JSON.stringify(oldItem));
                //console.log(newItem);
                newItem.current = (newItem.current - 0) + 1;
                newItem.target = newItem.target;
                //console.log(newItem);
                SpecializationItem.findOneAndUpdate({
                    name: newItem.name
                }, newItem, function(err, spcItem) {
                    if (err) return handleError(err);
                    //console.log('Mi item es: ' + spcItem);
                    socket.emit('update');
                })

                //InternshipItem.find({ ref: data.ref }, function(err, existingItem) {
                console.log('dentro!');
                if (err) return console.log(err);
                console.log(existingItem);
                //if (existingItem) return socket.emit('update:internship', 'Error: internship already exchanged');
                console.log('El user es ' + data.member);
                var intItem = {
                    ref: data.ref,
                    exchanged: true,
                    member: data.member
                }
                console.log('El item q vamos a enviar es:')
                console.log(intItem);

                InternshipItem.findOneAndUpdate({
                    ref: intItem.ref
                }, intItem, {
                    upsert: true
                }, function(err, updatedItem) {
                    if (err) return console.log(err);;
                    console.log('Mi item es: ' + updatedItem);
                    socket.emit('get:internship', intItem.ref);
                })

            })
        })




        //socket.in(msg.room).emit('update');
    });



    socket.on('edit:specialization', function(item) {
        console.log(item);
        var editedItem = {
            name: item.name,
            current: item.current,
            target: item.target
        };

        SpecializationItem.findOneAndUpdate({
            name: editedItem.name
        }, editedItem, function(err, spcItem) {
            if (err) console.log(err);
            console.log('Mi item es actualizado es: ' + spcItem);
            socket.emit('update');
        })
    });

    socket.on('get:internships', function() {

        InternshipItem.find(function(err, intItems) {
            if (err) console.log(err);
            console.log('Mi array  es: ' + intItems);
            socket.emit('intList', intItems);

        })
    });

    socket.on('get:internship', function(ref) {
        console.log('La referencia es: ' + ref);
        if (ref === '') return socket.emit('intItem', false);
        InternshipItem.findOne({
            ref: ref
        }, function(err, intItem) {
            if (err) console.log(err);
            console.log('Mi práctica  es: ' + intItem);
            socket.emit('intItem', true, intItem);

        })
    });

    socket.on('update:internship', function(internship) {

        //InternshipItem.findOne({ ref: internship.ref }, function(err, intItem) {
        //if (err) console.log(err);
        console.log(internship.exchanged);
        if (internship.exchanged) {
            //intItem.exchanged = internship.exchanged;
            InternshipItem.findOneAndUpdate({
                ref: internship.ref
            }, internship, {
                upsert: true
            }, function(err, newItem) {
                if (err) console.log(err);
                console.log('añadida');
            })
        } else {
            console.log('va a borrar');
            //intItem.exchanged = internship.exchanged;
            InternshipItem.findOneAndRemove({
                ref: internship.ref
            }, function(err, oldItem) {
                if (err) console.log(err);
                console.log('No borra, pero encuentra:');
                console.log(oldItem);
            })
        }

        //})
    });



    /*var chatMessage = mongoose.model('chatMessage', {
        'room': String,
        'user': String,
        'text': String,
        'time': String
    });
    var newMsg = new chatMessage(msg);
    newMsg.save(function(err) {
        if (err) {
            console.log(err);
        } else {
            console.log('meow');
        }
    });*/
});